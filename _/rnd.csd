<CsoundSynthesizer>

<CsOptions>
</CsOptions>

<CsInstruments>
ksmps=32

instr 123
	prints "Hello World!%n"
	aSin oscils 0dbfs/4, p4, 0
	out aSin
endin

instr 1
	seed 0

  irand random 0, 100

  ifreq = 880

  if (irand < 80) then
    ifreq = 440
  endif

	aOut oscil 0dbfs/4, ifreq
	out aOut
endin

</CsInstruments>

<CsScore>
i 1 0 1
i 1 1 1
i 1 2 1
i 1 3 1
i 1 4 1
i 1 5 1
e
</CsScore>
