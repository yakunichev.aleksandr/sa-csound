<CsoundSynthesizer>

<CsOptions>
-odac
</CsOptions>

<CsInstruments>
ksmps=32

instr 123
	prints "Hello World!%n"
	aSin	 oscils 0dbfs/4, p4, 0
	out aSin
endin

instr 1
	kFreq init 0
	iMax = 2000
	kFreq = kFreq + 0.2
	; kFreq2 = sin(1.5707963 * kFreq) * kFreq ; easeInSine
	; kFreq2 = (1 + sin(1.5707963 * (kFreq - 1))) * kFreq ; easeOutSine
 	; kFreq2 = (0.5 * (1 + sin(3.1415926 * (kFreq - 0.5)))) * kFreq ; easeInOutSine
	; kFreq2 = (kFreq * kFreq) * kFreq ; easeInQuad
	; kFreq2 = (kFreq * (2 - kFreq)) * kFreq ; easeOutQuad
	; kFreq2 = (sqrt(kFreq)) * kFreq ; easeInOutQuad

	kFreq min kFreq, iMax

 iAtt = 0.7
 iDec = 0.5
 iSus = 0.7
 iRel = 1
 kEnv madsr iAtt, iDec, iSus, iRel

	aOut oscil 0dbfs/4, kFreq2
	out aOut*kEnv
endin

</CsInstruments>

<CsScore>
i 1 0 5
i 1 0 5
e
</CsScore>
