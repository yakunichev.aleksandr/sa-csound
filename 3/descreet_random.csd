<CsoundSynthesizer>

<CsOptions>
-odac
</CsOptions>

<CsInstruments>
sr = 44100
ksmps = 128
nchnls = 2
0dbfs = 1

instr trigger_note
  seed 0
  kTrig metro 2

  if kTrig == 1 then
    kLine[] init 3
    kLine[] fillarray .2, .5, .3
    kVal random 0, 1
    kIndex = 0
    kAccum = kLine[0]

    // Find correct index in array
    while kVal >= kAccum do
      kIndex += 1
      kAccum += kLine[kIndex]
    od

    printk2 kIndex
  endif
endin

</CsInstruments>

<CsScore>
i "trigger_note" 0 600
</CsScore>

</CsoundSynthesizer>
