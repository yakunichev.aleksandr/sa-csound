<CsoundSynthesizer>
<CsOptions>
-odac
</CsOptions>

; ==============================================
<CsInstruments>

giA init 0
giB init 1
giC init 2
giD init 3

gkVal init 0

instr 1
  iMarkov[][] init 4, 4
  /*                A    B    C    D  */
  iMarkov fillarray 0.5, 0.5, 0,   0, /* A */
                    1,   0,   0,   0, /* B */
                    0.7, 0.2, 0.1, 0, /* C */
                    0,   0,   1,   0  /* D */

  kRes = 0
  kX = 0

  iRnd random 0, 1

  until kX == lenarray(iMarkov) do
    kV1 = 0
    kV2 = 0

    if (kX == 0) then
      kV1 = 0
    elseif (kX != 0) then
      kV1 = iMarkov[gkVal][kX]
    endif

    if (kX == 3) then
      kV2 = 1
    elseif (kX != 3) then
      kV2 = iMarkov[gkVal][kX + 1]
    endif

    if (iRnd > kV1 && iRnd < kV2) then
      printf "\nValue: %f, %f\n", kX + 1, gkVal, kX
    endif

    kX += 1
  od

  /* printf "%s", 1, "\Init instr 1:\n" */

  /* kX = 0 */
  /* until kX == 4 do */
  /*   kY = 0 */

  /*   until kY == 4 do */
  /*     printf "iMarkov[%d][%d] = %f\n", kY + 1 + kX + 1, kX, kY, iMarkov[kX][kY] */
  /*     kY += 1 */
  /*   od */

  /*   printf "\n", kX + 1 */

  /*   kX += 1 */
  /* od */

  /* aout oscil 0dbfs/4, 440 */
  /* out aout */
endin

</CsInstruments>
; ==============================================

<CsScore>
i 1 0 3
e
</CsScore>

</CsoundSynthesizer>
