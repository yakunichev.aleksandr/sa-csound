<CsoundSynthesizer>
<CsOptions>
-odac
</CsOptions>

; ==============================================
<CsInstruments>

instr 1
  kI init 0
  kI += 2

  kFreq = ((1 + sin(1.5707963 * (kI - 1))) * kI) + 200

  aout oscil 0dbfs/4, kFreq
  out aout
endin

</CsInstruments>
; ==============================================

<CsScore>
i 1 0 3
e
</CsScore>

</CsoundSynthesizer>
