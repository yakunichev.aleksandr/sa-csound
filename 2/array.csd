<CsoundSynthesizer>
<CsOptions>
-odac
</CsOptions>

; ==============================================
<CsInstruments>

instr 1
  iMarkov[][] init 4, 4
  iMarkov fillarray 0.5, 0.5, 0,   0,
                    1,   0,   0,   0,
                    0.7, 0.2, 0.1, 0,
                    0,   0,   1,   0

  printf "%s", 1, "\nInitial content:\n"
  printf "iMarkov[0][0] %f", 1, iMarkov[0][0]
  printf "%s", 1, "\n\n"

  kX = 0
  until kX == 4 do
    kY = 0

    until kY == 4 do
      printf "iMarkov[%d][%d] = %f\n", kY + 1 + kX + 1, kX, kY, iMarkov[kX][kY]
      kY += 1
    od

    printf "\n", kX + 1

    kX += 1
  od

  aout oscil 0dbfs/4, 440
  out aout
endin

</CsInstruments>
; ==============================================

<CsScore>
i 1 0 3
e
</CsScore>

</CsoundSynthesizer>
